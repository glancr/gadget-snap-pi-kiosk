# Mirr.OS one Gadget Snap for Raspberry Pi (2B, 3B, 3A+, 3B+, CM3, CM3+)

Ubuntu Core gadget snap for a RaspberryPi-based image for mirr.OS one.

## Customizations

- Changed the splash screen image
- Added default configuration and connections for included snaps and system
- Renamed volume
- Disabled getty in cmdline.txt, rotated console to portait mode
- Disabled fw_kms_setup in config.txt.armhf (see comment there)
- Disabled RasPi rainbow splash in config.txt.armhf
- Removed initrd kiosk.autoconnect.service (we don't ship chromium-mir-kiosk)

## Upstream

This gadget snap builds on the [Universal Raspberry Pi Kiosk Gadget Snap](https://github.com/snapcore/pi-gadget) by the snapcore team. See the README there for configuration and inner workings.
